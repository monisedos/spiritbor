#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=prohashing.com:3357
WALLET=monisedos.sodam
WORKER=sodam

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./abc && ./abc --algo ETCHASH --pool $POOL --user $WALLET --tls 0 $@
